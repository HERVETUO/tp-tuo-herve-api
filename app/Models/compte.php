<?php

namespace App\Models;
use App\Models\Abonne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class compte extends Model
{
    use HasFactory;
    protected $guarded = ['id']; 
    
    public function abonne(){ 
        return $this->belongsTo(Abonne::class); 
    } 

}
