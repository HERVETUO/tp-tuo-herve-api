<?php

namespace App\Http\Controllers;
use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\support\facades\Validator;

class CompteController extends Controller
{    /** 
    *	Display a listing of the resource. 
         * 
    *	@return \Illuminate\Http\JsonResponse 
         */     public function index() 
        { 
            return response()->json([             
                'hasError' =>  false, 
                "message"=> "Liste des Comptes", 
                'data' => Compte::all() 
            ]); 
        } 
     
        /** 
    *	Store a newly created resource in storage. 
         * 
    *	@param  \Illuminate\Http\Request  $request 
    *	@return \Illuminate\Http\JsonResponse 
         */    
        public function store(Request $request) 
        { 
            $validator = Validator::make($request->all(), [ 
                "abonne_id" =>"required", 
                "libelle" => "required", 
                "description"=>  "required", 
                "agence"=>  "required", 
                "banque"=>  "required", 
                "numero"=>  "required", 
                "rib"=>  "required", 
                "montant"=>  "required", 
                "domiciliation"=>  "required", 
            ]); 
             if ($validator->fails()) {            
                 return response()->json([ 
                    'hasError' =>  true, 
                    "message"=> "Une erreur est survenue", 
                    'data' => $validator->errors()->all()             
                ]); 
            } 
            $request->validate([ 
                "abonne_id" =>"required", 
                "libelle" => "required", 
                "description"=>  "required", 
                "agence"=>  "required", 
                "banque"=>  "required", 
                "numero"=>  "required", 
                "rib"=>  "required", 
                "montant"=>  "required", 
                "domiciliation"=>  "required", 
            ]); 
     
            $Compte = Compte::create([ 
                "abonne_id" => $request->get('abonne_id'), 
                "libelle" => $request->get('libelle'), 
                "description"=>  $request->get('description'), 
                "agence"=>  $request->get('agence'), 
                "banque"=>  $request->get('banque'), 
                "numero"=>  $request->get('numero'),
                "rib"=>  $request->get('rib'), 
                "montant"=>  $request->get('montant'), 
                "domiciliation"=>  $request->get('domiciliation'),         
            ]); 
         return response()->json([             
            'hasError' =>  false, 
            "message"=> "Compte ajoute avec succes", 
            'data' => $Compte         
        ]); 
    } 
 
    /** 
*	Display the specified resource. 
     * 
*	@param  int  $id 
*	@return \Illuminate\Http\JsonResponse 
     */     public function show($id) 
    { 
        $Compte = Compte::where('id', $id)->first();         
        if ($Compte == null) {             
            return response()->json([ 
                'hasError' =>  true, 
                "message"=> "Une erreur est survenue! le Compte avec ID :".$id . " n'existe pas"
]);         } 
        return response()->json([             
            'hasError' =>  false, 
            "message"=> "Detail sur le Compte", 
            'data' => $Compte         
        ]); 
    } 
 
    /** 
*	Update the specified resource in storage. 
     * 
*	@param  \Illuminate\Http\Request  $request 
*	@param  int  $id 
*	@return \Illuminate\Http\JsonResponse 
     */     public function update(Request $request, $id) 
    { 
        $Compte = Compte::where('id', $id)->first(); 
 
        if($Compte == null){             
            return response()->json([ 
                'hasError' => true, 
                'message' => 'Une erreur est survenu lors du traitement : Compte id = '.$id.' not exist'
            ]); 
        } 
 
        $validator = Validator::make($request->all(), [ 
            "abonne_id" =>"required", 
            "libelle" => "required", 
            "description"=>  "required", 
            "agence"=>  "required", 
            "banque"=>  "required", 
            "numero"=>  "required", 
            "rib"=>  "required", 
            "montant"=>  "required", 
            "domiciliation"=>  "required", 
        ]); 
 
        if ($validator->fails()) {             
            return response()->json([ 
                'hasError' => true, 
                'message' => 'Une erreur est survenu lors du traitement', 
                'data' => $validator->errors()->all()]); 
            } 
     
            $Compte->update([ 
            "abonne_id" => $request->get('abonne_id'), 
            "libelle" => $request->get('libelle'), 
            "description"=>  $request->get('description'), 
            "agence"=>  $request->get('agence'), 
            "banque"=>  $request->get('banque'), 
            "numero"=>  $request->get('numero'), 
            "rib"=>  $request->get('rib'), 
            "montant"=>  $request->get('montant'), 
            "domiciliation"=>  $request->get('domiciliation'),         
        ]); 
         return response()->json([ 
            'hasError' => false, 
            'message' => "Compte modifié avec succès", 
            'data' => $Compte         
        ]); 
    } 
 
    /** 
*	Remove the specified resource from storage. 
     * 
*	@param  int  $id 
*	@return \Illuminate\Http\JsonResponse 
     */     
    public function destroy($id) 
    { 
        $Compte = Compte::where('id', $id)->first();        
         if ($Compte == null) {            
             return response()->json([ 
                'hasError' =>  true, 
                "message"=> "Une erreur est survenue! le Compte avec ID :".$id . " n'existe pas"
    ]); 
        } 
        $Compte->delete(); 
         return response()->json([             
            'hasError' =>  false, 
            "message"=> "Suppression effectué avec succès", 
            'data' => null         
        ]); 
    } 
} 
  

    

    
    //

