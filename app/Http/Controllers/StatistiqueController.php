<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatistiqueController extends Controller
{    /** 
    *	Display the specified resource. 
         * 
    *	@param  int  $id 
    *	@return \Illuminate\Http\JsonResponse 
         */     public function index() 
        {          
            return response()->json([ 
                "NOMBRE DE COMPTES DANS LA BASE" => Compte::all()->count(), 
                "NOMBRE D'ABONNES DANS LA BASE" => Abonne::all()->count(), 
            ]); 
        }  
        public function statsByAbonneId($id) 
        { 
            $statistique = []; 
            $abonnes = Abonne::where('id', $id)->withCount('comptes')->get();         foreach ($abonnes as $abonne) { 
                $statistique[] = [ 
                    $abonne->nom.' '.$abonne->prenom => $abonne->comptes_count . ' COMPTES', 
                ]; 
            } 
     
            return response()->json([ 
                'ABONNÉES ASSOCIE A COMBIEN DE COMPTE' => $statistique         ]); 
        } 
    } 
      
    
    

