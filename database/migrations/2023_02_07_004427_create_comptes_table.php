<?php
// namespace App\Http\Controllers;

use App\Models\Abonne;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comptes', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Abonne::class) 
                ->constrained() 
                ->onUpdate('cascade') 
                ->onDelete('cascade'); 
            $table->string('libelle'); 
            $table->string('description'); 
            $table->string('agence', 5); 
            $table->string('banque',5); 
            $table->string('numero', 11)->unique(); 
            $table->string('rib', 2); 
            $table->double('montant'); 
            $table->string('domiciliation'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comptes');
    }
};
